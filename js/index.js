$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#informacion').on('show.bs.modal',function(e){
      console.log('el modal contacto se esta mostrando');
      
      $('#informacionBtn').removeClass('btn-outline-dark');
      $('#informacionBtn').addClass('btn-primary');
      $('#informacionBtn').prop('disabled', true);
    });

    $('#informacion').on('shown.bs.modal',function(e){
      console.log('el modal contacto se mostró');
    });
    
    $('#informacion').on('hide.bs.modal',function(e){
      console.log('el modal contacto se esta ocultando');
    });
    $('#informacion').on('hidden.bs.modal',function(e){
      console.log('el modal contacto se oculto');
      $('#informacionBtn').removeClass('btn-primary');
      $('#informacionBtn').addClass('btn-outline-dark');
      $('#informacionBtn').prop('disabled', false);
    });
  });